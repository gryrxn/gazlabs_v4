#!/bin/bash

#Stop services for cleanup
systemctl stop rsyslog

# get rid of all snap stuff
snap remove lxd
snap remove core18
snap remove snapd
apt purge snapd -y
rm -rf ~/snap
rm -rf /snap
rm -rf /var/snap
rm -rf /var/lib/snapd

#clear audit logs
if [ -f /var/log/audit/audit.log ]; then
    truncate -s0 > /var/log/audit/audit.log
fi
if [ -f /var/log/wtmp ]; then
    truncate -s0 > /var/log/wtmp
fi
if [ -f /var/log/lastlog ]; then
    truncate -s0 > /var/log/lastlog
fi

#cleanup /tmp directories
rm -rf /tmp/*
rm -rf /var/tmp/*

# cleanup log files 
find /var/log/ -type f -exec truncate -s0 {} \;

#cleanup current ssh keys
rm -f /etc/ssh/ssh_host_*

#add check for ssh keys on reboot...regenerate if neccessary
cat << 'EOL' > /etc/rc.local 
#!/bin/sh
if [ ! -e /etc/ssh/ssh_host_rsa_key ]; then
  dpkg-reconfigure openssh-server
  systemctl restart ssh
fi
exit 0
EOL

# make sure the script is executable
chmod +x /etc/rc.local

# reset hostname
truncate -s0 /etc/hostname
hostnamectl set-hostname localhost

# reset machine ID
truncate -s0 /etc/machine-id

# cleanup apt
apt-get autoremove -y
apt-get clean


# cleans out all of the cloud-init cache / logs - this is mainly cleaning out networking info
cloud-init clean --logs

# cleanup shell history
truncate -s0 > ~/.bash_history && history -c
history -w

# shutdown
systemctl poweroff
