#!/bin/bash
kubectl apply -f gcr-image-pull-secret.yaml
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gcr-image-pull"}]}'
