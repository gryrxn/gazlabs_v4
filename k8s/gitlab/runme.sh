#!/bin/bash
kubectl apply -f gitlab-admin-service-account.yaml 
kubectl apply -f registry-pull-secret.yaml
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gitlab-registry-pull"}]}'
