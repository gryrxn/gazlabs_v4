#!/bin/bash

docker run -ti --rm \
    -v "$(pwd)/letsencrypt:/etc/letsencrypt" \
    -v "$(pwd)/cloudflare.ini:/cloudflare.ini" \
    certbot/dns-cloudflare:latest \
    certonly \
    --dns-cloudflare \
    --dns-cloudflare-credentials /cloudflare.ini \
    -d "*.garyrixon.co.uk" \
    -d "*.kw.garyrixon.co.uk" \
    --email gryrxn@gmail.com \
    --agree-tos \
    --server https://acme-v02.api.letsencrypt.org/directory
