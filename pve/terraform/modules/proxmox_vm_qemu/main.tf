terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = " 2.7.1"
    }
  }
}

resource "proxmox_vm_qemu" "proxmox_vm_qemu" {
  additional_wait        = 15
  agent                  = 1
  balloon                = 1
  bios                   = "ovmf"
  boot                   = "c"
  bootdisk               = "virtio0"
  clone                  = var.clone_source
  clone_wait             = 15
  cores                  = var.cores
  cpu                    = "host"
  define_connection_info = false
  force_create           = false
  full_clone             = false
  hotplug                = "cpu,disk,memory,network,usb"
  ipconfig0    = var.ipconfig0
  ipconfig1    = var.ipconfig1
  kvm          = true
  memory       = var.memory
  name         = var.name
  nameserver   = var.nameserver
  numa         = true
  onboot       = var.onboot
  preprovision = false
  qemu_os      = "other" # we want this to be q35, but when we set it to that here, the provider sets it to other. so we set to other here to avoid perpetual changes in plans.
  scsihw       = "virtio-scsi-pci"
  searchdomain = var.searchdomain
  sockets      = 2
  sshkeys     = var.sshkeys
  target_node = var.target_node
  vmid = var.vmid


  disk {
    backup = 1
    cache  = "none"
    discard = "on"
    format = "qcow2"
    iothread    = 1
    mbps        = 0
    mbps_rd     = 0
    mbps_rd_max = 0
    mbps_wr     = 0
    mbps_wr_max = 0
    media       = ""
    replicate   = 1
    size        = var.disk_size
    slot = 0
    storage = "pve"
    type   = "virtio"
    volume = ""
  }

  network {
    bridge   = var.network_1_bridge
    firewall = false
    link_down = var.network_1_link_down
    macaddr   = ""
    model     = "virtio"
    queues    = 0
    rate      = 0
    tag       = var.network_1_tag
  }

    network {
    bridge   = var.network_2_bridge
    firewall = false
    link_down = var.network_2_link_down
    macaddr   = ""
    model     = "virtio"
    queues    = 0
    rate      = 0
    tag       = var.network_2_tag
  }

  # Don't migrate VMs if they're on a different node to the one specified in the resource
  lifecycle {
    ignore_changes = [
      target_node,
      clone_wait,
      additional_wait,
      force_create,
      full_clone,
      define_connection_info,
      preprovision,
    ]
  }
}
