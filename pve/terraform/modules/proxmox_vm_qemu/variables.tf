variable "clone_source" {
  description = "The name of the VM we want to create a linked clone of"
  default     = "focal-template-2020-04-23"
}

variable "cores" {
  description = "The number of CPU cores"
  default     = 1
}

variable "ipconfig0" {
  description = "The network config for the primary network adapter in form 'ip=x.x.x.x/x,gw=x.x.x.x'"
  default     = "ip=10.2.0.2/24,gw=10.2.0.254"
}

variable "ipconfig1" {
  description = "The network config for the primary network adapter in form 'ip=x.x.x.x/x,gw=x.x.x.x'"
  default     = "ip=10.2.0.2/24,gw=10.2.0.254"
}

variable "memory" {
  description = "The amount of RAM in MB"
  default     = "1024"
}

variable "name" {
  description = "The name of the VM"
  default     = "tf-vm.kw.garyrixon.co.uk"
}

variable "nameserver" {
  description = "The default DNS nameserver"
  default     = "10.2.0.254"
}

variable "onboot" {
  description = "Whether or not to start the VM when the node boots"
  default     = false
}

variable "searchdomain" {
  description = "The DNS search domain"
  default     = "kw.garyrixon.co.uk"
}

variable "sshkeys" {
  description = "The SSH public key to be inserted into the authorized_keys file of the primary user"
  default     = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBQLurfD19tT1xhNJFrG5gXhlJ6d3zhsgaHIEWU3wDak ubuntu@vm"
}

variable "target_node" {
  description = "The node the VM will be created on"
  default     = "pve0"
}

variable "disk_size" {
  description = "The size of the primary disk in GB"
  default     = "8G"
}

variable "network_1_bridge" {
  description = "The network bridge the primary network adapter will be connected to"
  default     = "vmbr1"
}

variable "network_1_tag" {
  description = "The VLAN tag of the primary network adapter"
  default     = 2
}

variable "network_1_link_down" {
  description = "Enable/disable network link"
  default = false
}

variable "network_2_bridge" {
  description = "The network bridge the primary network adapter will be connected to"
  default     = "vmbr1"
}

variable "network_2_tag" {
  description = "The VLAN tag of the secondary network adapter"
  default     = 2
}

variable "network_2_link_down" {
  description = "Enable/disable network link"
  default = true
}

variable "vmid" {
  description = "Th numeric ID of the VM"
  default     = "000"
}
