module "k8s0_kw_garyrixon_co_uk" {
  source         = "./modules/proxmox_vm_qemu"
  clone_source   = "focal-template-2021-03-25"
  cores          = 4
  ipconfig0      = "ip=10.0.40.10/24,gw=10.0.40.254"
  ipconfig1      = "ip=10.0.34.100/24,gw=10.0.34.254"
  memory         = 16384
  name           = "k8s0.kw.garyrixon.co.uk"
  nameserver     = "10.0.40.254"
  onboot         = true
  searchdomain   = "kw.garyrixon.co.uk"
  target_node    = "pve0"
  disk_size      = "64G"
  network_1_bridge = "vmbr1"
  network_1_tag    = 40
  network_2_bridge = "vmbr1"
  network_2_tag    = 34
}

#module "k8s1_kw_garyrixon_co_uk" {
#  source         = "./modules/proxmox_vm_qemu"
#  clone_source   = "focal-template-2021-03-25"
#  cores          = 4
#  ipconfig0      = "ip=10.0.40.11/24,gw=10.0.40.254"
#  memory         = 16384
#  name           = "k8s1.kw.garyrixon.co.uk"
#  nameserver     = "10.0.40.254"
#  onboot         = true
#  searchdomain   = "kw.garyrixon.co.uk"
#  target_node    = "pve1"
#  disk_size      = "64G"
#  network_1_bridge = "vmbr1"
#  network_1_tag    = 40
#  network_2_bridge = "vmbr1"
#  network_2_tag    = 34
#}
#
#module "k8s2_kw_garyrixon_co_uk" {
#  source         = "./modules/proxmox_vm_qemu"
#  clone_source   = "focal-template-2021-03-25"
#  cores          = 4
#  ipconfig0      = "ip=10.0.40.12/24,gw=10.0.40.254"
#  memory         = 16384
#  name           = "k8s2.kw.garyrixon.co.uk"
#  nameserver     = "10.0.40.254"
#  onboot         = true
#  searchdomain   = "kw.garyrixon.co.uk"
#  target_node    = "pve2"
#  disk_size      = "64G"
#  network_1_bridge = "vmbr1"
#  network_1_tag    = 40
#  network_2_bridge = "vmbr1"
#  network_2_tag    = 34
#}

