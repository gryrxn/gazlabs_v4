terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = " 2.7.1"
    }
  }
}

provider "proxmox" {
  pm_api_url  = "https://pve0.kw.garyrixon.co.uk:8006/api2/json"
  pm_user     = "root@pam"
  pm_password = var.pm_password
}

