#!/bin/bash

ssh-keygen -f "/home/gary/.ssh/known_hosts" -R "pve0.kw.gazlabs.io"
ssh-keygen -f "/home/gary/.ssh/known_hosts" -R "pve1.kw.gazlabs.io"
ssh-keygen -f "/home/gary/.ssh/known_hosts" -R "pve2.kw.gazlabs.io"

ssh-copy-id -f -i pve.pub root@pve0.kw.gazlabs.io
ssh-copy-id -f -i pve.pub root@pve1.kw.gazlabs.io
ssh-copy-id -f -i pve.pub root@pve2.kw.gazlabs.io

ssh root@pve0.kw.gazlabs.io passwd
ssh root@pve1.kw.gazlabs.io passwd
ssh root@pve2.kw.gazlabs.io passwd
