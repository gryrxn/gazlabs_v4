#!/bin/bash

set -eu

vmid="1000"
storage=${1}
disk="virtio0"
ubuntu_release="focal"
img_name=${ubuntu_release}-server-cloudimg-amd64

#wget https://cloud-images.ubuntu.com/focal/current/${img_name}.img
qm create ${vmid}
qm set ${vmid} --name ${ubuntu_release}-template-$(date +%F)

qm importdisk ${vmid} ${img_name}.img ${storage} > /dev/null
qm set ${vmid} --scsihw virtio-scsi-pci --${disk} ${storage}:vm-${vmid}-disk-0
qm set ${vmid} --${disk} ${storage}:vm-${vmid}-disk-0,discard=on,iothread=1
qm resize ${vmid} ${disk} 8192M
qm set ${vmid} --boot c --bootdisk ${disk}
qm set ${vmid} --ide0 ${storage}:cloudinit
qm set ${vmid} --efidisk0 ${storage}:1

qm set ${vmid} --memory 1024
qm set ${vmid} --cpu cputype=host
qm set ${vmid} --numa 1
qm set ${vmid} --sockets 2
qm set ${vmid} --cores 1
qm set ${vmid} --serial0 socket --vga serial0
qm set ${vmid} --net0 model=virtio,bridge=vmbr2,tag=2
qm set ${vmid} --agent enabled=1,fstrim_cloned_disks=1
qm set ${vmid} --bios ovmf
qm set ${vmid} --machine q35

qm set ${vmid} --citype nocloud
qm set ${vmid} --ipconfig0 ip=10.0.2.2/24,gw=10.0.2.254
qm set ${vmid} --nameserver 10.0.2.254
qm set ${vmid} --searchdomain kw.garyrixon.co.uk
qm set ${vmid} --sshkeys vm.pub 

qm set ${vmid} --protection 1
